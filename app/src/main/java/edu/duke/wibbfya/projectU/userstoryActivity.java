package edu.duke.wibbfya.projectU;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

public class userstoryActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextInputLayout mUserStory;
    private Button mSaveBtn;
    private String projectID;
    private DatabaseReference mProjectsDatabase;
    private Projects retrievedProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userstory);
        mToolbar = (Toolbar) findViewById(R.id.userstory_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("User Story");
        mUserStory=(TextInputLayout)findViewById(R.id.UserStory);
        mSaveBtn=(Button)findViewById(R.id.userstory_save_btn);

        projectID = getIntent().getStringExtra("projectID");
        mProjectsDatabase = FirebaseDatabase.getInstance().getReference().child("Project").child(projectID);

        mProjectsDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                retrievedProject = dataSnapshot.getValue(Projects.class);
                mUserStory.getEditText().setText(retrievedProject.getUserStory());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userStory = mUserStory.getEditText().getText().toString();

                mProjectsDatabase.child("userStory").setValue(userStory);

            }
        });


    }
}
