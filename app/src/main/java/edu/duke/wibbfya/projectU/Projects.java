package edu.duke.wibbfya.projectU;

/**
 * Created by XL on 4/8/18.
 */

class Projects {
    // Fields
    public String projectName;
    public String startDate;
    public String endDate;
    public String description;
    public String owner;
    public String functionalReq;
    public String nonFunctionalReg;
    public String userStory;
    public String sprint1;
    public String sprint2;
    public String sprint3;
    public String meeting;

    // Constructor
    public Projects(){

    }

    // Methods
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOwner(String owner) {this.owner = owner;}

    public void setFunctionalReq(String functionalReq) {this.functionalReq = functionalReq;}

    public void setNonFunctionalReg(String nonFunctionalReg) {
        this.nonFunctionalReg = nonFunctionalReg;
    }

    public void setUserStory(String userStory) {this.userStory = userStory;}

    public void setSprint1(String sprint1) {this.sprint1 = sprint1;}

    public void setSprint2(String sprint2) {this.sprint2 = sprint2;}

    public void setSprint3(String sprint3) {this.sprint3 = sprint3;}
    public void setMeeting(String meeting) {this.meeting=meeting;}

    public String getProjectName() {return projectName;}

    public String getDescription() {return description;}

    public String getStartDate() {return startDate;}

    public String getEndDate() {return endDate;}

    public String getOwner() {return owner;}

    public String getFunctionalReq() {return functionalReq;}

    public String getNonFunctionalReg() {return nonFunctionalReg;}

    public String getUserStory() {return userStory;}

    public String getSprint1() {return sprint1;}

    public String getSprint2() {return sprint2;}

    public String getSprint3() {return sprint3;}

    public String getMeeting(){return meeting;}
}
