package edu.duke.wibbfya.projectU;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailActivity extends AppCompatActivity {

    private TextView name,briefIntro,ownerField;
    private Button documentation,testing,development,member,meeting;
    private Toolbar mToolbar;

    // Holder for the detail of the project passed from RequestFragment
    private String projectName, description, owner, id;
    private DatabaseReference mProjectsDatabase;
    private Projects retrievedProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        name=(TextView)findViewById(R.id.detail_displayName);
        briefIntro=(TextView)findViewById(R.id.detail_intro);
        documentation=(Button)findViewById(R.id.detail_doc_btn);
        development=(Button)findViewById(R.id.detail_development_btn);
        testing=(Button)findViewById(R.id.detail_testing_btn);
        ownerField=(TextView)findViewById(R.id.detail_owner);
        member=(Button)findViewById(R.id.detail_members_btn);
        meeting=(Button)findViewById(R.id.detail_chat_btn);

        member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent memberIntent=new Intent(DetailActivity.this,UsersActivity.class);
                startActivity(memberIntent);
            }
        });

        // Find the project
        mProjectsDatabase = FirebaseDatabase.getInstance().getReference().child("Project");

        // Get the project id passed from request fragment
        id = getIntent().getStringExtra("projectID");

        mProjectsDatabase.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                retrievedProject = dataSnapshot.getValue(Projects.class);
                projectName = retrievedProject.getProjectName();
                description = retrievedProject.getDescription();
                owner = retrievedProject.getOwner();

                // Set the project name and the info to display
                name.setText(projectName);
                briefIntro.setText(description);
                ownerField.setText("Project Owner: "+ owner);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        documentation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent docIntent=new Intent(DetailActivity.this,DocumentationActivity.class);
                docIntent.putExtra("projectID", id);
                startActivity(docIntent);

            }
        });

        development.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent devIntent=new Intent(DetailActivity.this,DevelopmentActivity.class);
                devIntent.putExtra("projectID",id);
                startActivity(devIntent);
            }
        });

        testing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent testIntent = new Intent(DetailActivity.this, TestingActivity.class);
                startActivity(testIntent);
            }
        });
        meeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent meetingIntent=new Intent(DetailActivity.this,MeetingActivity.class);
                meetingIntent.putExtra("projectID", id);
                startActivity(meetingIntent);
            }
        });

    }
}
