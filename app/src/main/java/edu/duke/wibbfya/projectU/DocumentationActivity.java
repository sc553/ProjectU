package edu.duke.wibbfya.projectU;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DatabaseReference;

public class DocumentationActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private Button req,story;
    private Button planbtn;
    private String projectID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documentation);
        mToolbar = (Toolbar) findViewById(R.id.document_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("Documents");

        req=(Button)findViewById(R.id.doc_requirement_btn);
        story=(Button)findViewById(R.id.doc_story_btn);

        projectID = getIntent().getStringExtra("projectID");

        req.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reqIntent=new Intent(DocumentationActivity.this,requirementActivity.class);
                reqIntent.putExtra("projectID", projectID);
                startActivity(reqIntent);
            }
        });

        story.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent storyIntent=new Intent(DocumentationActivity.this,userstoryActivity.class);
                storyIntent.putExtra("projectID", projectID);
                startActivity(storyIntent);

            }
        });

        planbtn=(Button)findViewById(R.id.doc_plan_btn);
        planbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent planintent=new Intent(DocumentationActivity.this,PlanActivity.class);
                startActivity(planintent);
            }
        });
    }
}
