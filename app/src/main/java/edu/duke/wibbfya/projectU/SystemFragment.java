package edu.duke.wibbfya.projectU;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class SystemFragment extends Fragment {

private Button mFile;
private View mMainView;
    public SystemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mMainView= inflater.inflate(R.layout.fragment_system, container, false);
        mFile=(Button)mMainView.findViewById(R.id.system_file);
        mFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fileIntent=new Intent(getActivity(),FileActivity.class);
                startActivity(fileIntent);
            }
        });
        return mMainView;
    }

}
