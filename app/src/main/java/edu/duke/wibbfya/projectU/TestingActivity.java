package edu.duke.wibbfya.projectU;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class TestingActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private ViewPager mViewPager;
    private TestsPagerAdapter mTestsPagerAdapter;
    private TabLayout mTabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing);
        mToolbar = (Toolbar) findViewById(R.id.testing_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("Testing");

        mViewPager = (ViewPager) findViewById(R.id.testing_tabPager);
        mTestsPagerAdapter = new TestsPagerAdapter(getSupportFragmentManager());

        mViewPager.setAdapter(mTestsPagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.testing_tabs);
        mTabLayout.setupWithViewPager(mViewPager);
    }
}
