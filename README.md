# ProjectU
## Description
ProjectU is an Android based instant messgae application using Firebase as server designed for small agile development team.  
Integrated Function:  
- Contact lists
- Friend chat
- Create and manage the project
- Upload group files and edit project plan

## Installation
To play with our code, clone the source code to your local computer. There is a APK file inside, you can install this app in your android phone or android simulator. The test phone version we used is Nexus 5X API 24. Or you can also open the project in your android studio and run it.
## Documentation
Check out our design documentation
- [requirement](https://gitlab.oit.duke.edu/yw263/ProjectU/blob/master/ProjectDocs/ProjectU_Requirements.pdf)
- [User Story and Architecture](https://gitlab.oit.duke.edu/yw263/ProjectU/blob/master/ProjectDocs/ProjectU_UserStory_and_Architecture.pdf)
- [Project Plan](https://gitlab.oit.duke.edu/yw263/ProjectU/blob/master/ProjectDocs/ProjectU_Project_Plan.pdf)
- [Test Plan](https://gitlab.oit.duke.edu/yw263/ProjectU/blob/master/ProjectDocs/ProjectU_Test_Plan.pdf)

## User guide
- First, you should create a account with your email
- When login, you can find all user and your profile on the right corner
- You can either send a friend request to a user on the all user view and accept the request in the same way
- You can find your friend in your contact list and chat with him/her
- You can create a new project in your profile and add description with it
- You can enter the project you created and change the project document like requirement, user story, project plan
- You can also upload some project file into the cloud
- have fun with our app

##  References
Since we used Firebase to handle our server part, you can check around the [Firebase documentation](https://firebase.google.com/docs/android/setup), in case you are confused when reading the code.