package edu.duke.wibbfya.projectU;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DevelopmentActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private Button mSaveBtn;
    private EditText mS1;
    private EditText mS2;
    private EditText mS3;
    private String projectID;
    private DatabaseReference mProjectsDatabase;
    private Projects retrievedProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_development);
        mToolbar = (Toolbar) findViewById(R.id.development_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("Software Development");
        mSaveBtn=(Button)findViewById(R.id.development_save_btn);
        mS1=(EditText)findViewById(R.id.s1txt);
        mS2=(EditText)findViewById(R.id.s2txt);
        mS3=(EditText)findViewById(R.id.s3txt);

        projectID = getIntent().getStringExtra("projectID");
        mProjectsDatabase = FirebaseDatabase.getInstance().getReference().child("Project").child(projectID);

        mProjectsDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                retrievedProject = dataSnapshot.getValue(Projects.class);
                mS1.setText(retrievedProject.getSprint1());
                mS2.setText(retrievedProject.getSprint2());
                mS3.setText(retrievedProject.getSprint3());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sprint1 = mS1.getText().toString();
                String sprint2 = mS2.getText().toString();
                String sprint3 = mS3.getText().toString();
                mProjectsDatabase.child("sprint1").setValue(sprint1);
                mProjectsDatabase.child("sprint2").setValue(sprint2);
                mProjectsDatabase.child("sprint3").setValue(sprint3);
            }
        });
    }
}
