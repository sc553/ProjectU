package edu.duke.wibbfya.projectU;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class requirementActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private TextInputLayout mFuncReq;
    private TextInputLayout mNonFuncReq;
    private Button mSaveBtn;
    private String projectID;
    private DatabaseReference mProjectsDatabase;
    private Projects retrievedProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requirement);
        mToolbar = (Toolbar) findViewById(R.id.requirement_toolbar);
        mFuncReq = (TextInputLayout) findViewById(R.id.requirement_func);
        mNonFuncReq = (TextInputLayout) findViewById(R.id.requirement_nonfunc);
        mSaveBtn = (Button) findViewById(R.id.requirement_save_btn);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("Requirements");

        projectID = getIntent().getStringExtra("projectID");
        mProjectsDatabase = FirebaseDatabase.getInstance().getReference().child("Project").child(projectID);

        mProjectsDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                retrievedProject = dataSnapshot.getValue(Projects.class);
                mFuncReq.getEditText().setText(retrievedProject.getFunctionalReq());
                mNonFuncReq.getEditText().setText(retrievedProject.getNonFunctionalReg());
                String funcReqDebug = "The retrieved functional req is " + retrievedProject.getFunctionalReq();
                String nonFuncDebug = "The retrieved non-functional req is " + retrievedProject.getNonFunctionalReg();
                Log.d("Testing", funcReqDebug);
                Log.d("Testing", nonFuncDebug);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String funcReq = mFuncReq.getEditText().getText().toString();
                String nonFuncReq = mNonFuncReq.getEditText().getText().toString();
                mProjectsDatabase.child("functionalReq").setValue(funcReq);
                mProjectsDatabase.child("nonFunctionalReg").setValue(nonFuncReq);
            }
        });
    }
}
