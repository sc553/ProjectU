package edu.duke.wibbfya.projectU;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class PlanActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private Button mPlan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        mToolbar = (Toolbar) findViewById(R.id.plan_appBar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Project Plan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mPlan=(Button)findViewById(R.id.file_btn);
        mPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fileIntent=new Intent(PlanActivity.this,FileActivity.class);
                startActivity(fileIntent);

            }
        });

    }
}
