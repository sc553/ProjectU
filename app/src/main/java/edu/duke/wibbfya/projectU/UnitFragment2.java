package edu.duke.wibbfya.projectU;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class UnitFragment2 extends Fragment {
    private Button mFile;
    private View mMainView;

    public UnitFragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mMainView= inflater.inflate(R.layout.fragment_unit_fragment2, container, false);
        mFile=(Button)mMainView.findViewById(R.id.unit_file);
        mFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fileIntent=new Intent(getActivity(),FileActivity.class);
                startActivity(fileIntent);
            }
        });
        return mMainView;
    }

}
