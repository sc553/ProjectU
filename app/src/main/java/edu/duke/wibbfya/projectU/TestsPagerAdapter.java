package edu.duke.wibbfya.projectU;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


/**
 * Created by XL on 4/10/18.
 */

class TestsPagerAdapter extends FragmentPagerAdapter {
    public TestsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                UnitFragment2 unitFragment = new UnitFragment2();
                return unitFragment;

            case 1:
                ComponentFragment componentFragment = new ComponentFragment();
                return  componentFragment;

            case 2:
                SystemFragment systemFragment = new SystemFragment();
                return systemFragment;

            default:
                return  null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    public CharSequence getPageTitle(int position){

        switch (position) {
            case 0:
                return "UNIT";

            case 1:
                return "COMPONENT";

            case 2:
                return "SYSTEM";

            default:
                return null;
        }

    }
}
