package edu.duke.wibbfya.projectU;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MeetingActivity extends AppCompatActivity {

    private Button mSaveBtn;
    private TextInputLayout mMeeting;
    private Toolbar mToolbar;
    private String projectID;
    private DatabaseReference mProjectsDatabase;
    private Projects retrievedProject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting);

        mToolbar = (Toolbar) findViewById(R.id.meeting_appBar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Schedule Meeting");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mSaveBtn=(Button)findViewById(R.id.meeting_save_btn);
        mMeeting=(TextInputLayout)findViewById(R.id.meeting_input);
        projectID = getIntent().getStringExtra("projectID");
        mProjectsDatabase = FirebaseDatabase.getInstance().getReference().child("Project").child(projectID);

        mProjectsDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                retrievedProject = dataSnapshot.getValue(Projects.class);
                mMeeting.getEditText().setText(retrievedProject.getMeeting());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String meeting = mMeeting.getEditText().getText().toString();
                mProjectsDatabase.child("meeting").setValue(meeting);
            }
        });

    }
}
